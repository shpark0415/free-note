import React from 'react';
import Root from './routes';
import {Provider} from 'react-redux';
import configureStore from './store/config';

export const store = configureStore();

function App() {
    return (
        <Provider store={store}>
            <Root/> 
        </Provider>
    );
}

export default App;


