export type Response = {
	result: boolean
    message: string
}