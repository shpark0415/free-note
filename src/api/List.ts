import {get, del, postRevocable, getRevocable} from "../util/network";
import {AxiosResponse} from "axios";
import {Response} from "./Common";
import {PostItem} from "../types";
import {PreviewCardProps} from "../components/PreviewCard";
import {AlarmItem} from "../components/Alarm";

export type listType = PostItem & {preview: PreviewCardProps}
export type getListResponse = {
  list: Array<listType>,
  allCnt: number,
  endPage: boolean
}
// 전체목록
export const getList = (data: {
  page: number,
  keyword: string,
  category_idx: number
}): Promise<AxiosResponse<getListResponse>> => {
    return postRevocable({
      path: '/list/getList',
      data: data
    })
}

// 알람
export const getAlarm = (): Promise<AxiosResponse<Array<AlarmItem>>> => {
  return get({
    path: '/list/getAlarm'
  })
}

export type StorageList = listType & {isSelect: boolean}
export type getStarListResponse = {
  list: Array<StorageList>,
  allCnt: number,
  endPage: boolean
}

// 북마크목록
export const getStarLists = (data: {page: number}): Promise<AxiosResponse<getStarListResponse>> => {
    return getRevocable({
      path: '/list/getStarLists',
      data: data
    })
}

// 완료목록
export const getExpireLists = (data: {page: number}): Promise<AxiosResponse<getStarListResponse>> => {
  return getRevocable({
    path: '/list/getExpireLists',
    data: data
  })
}

// 완료선택 삭제
export const delExpireLists = (data: {list: Array<StorageList>}): Promise<AxiosResponse<getStarListResponse>> => {
  return del({
    path: '/list/delExpireLists',
    data: data
  })
}