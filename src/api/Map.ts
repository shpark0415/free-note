import { put, get, del, getRevocable } from "../util/network";
import { AxiosResponse } from "axios";
import { Response } from "./Common";
import { District } from "../types";

export type MarksResponse = {
	idx: number,
	post_idx: number, 
	title: string, 
	latitude: number, 
	longitude: number
}
export const getMarks = (): Promise<AxiosResponse<Array<MarksResponse>>> => {
	return getRevocable({
		path: '/map/getMarks',
	})
}

// 지역명 전체목록
export const getDistrict = (data: { keyword: string }): Promise<AxiosResponse<Array<District>>> => {
	return getRevocable({
		path: '/map/getDistrict',
		data: data
	})
}

type CoordinatesOptions = {
	latitude: number,
	longitude: number,
	zoom: number
}
export const getCoordinates = (): Promise<AxiosResponse<CoordinatesOptions>> => {
	return get({
		path: '/map/getCoordinates',
	})
}

export const setCoordinates = (data: {
	latitude: number,
	longitude: number,
	zoom: number
}): Promise<AxiosResponse<Response>> => {
	return put({
		path: '/map/setCoordinates',
		data: data
	})
}