import {put, get, del, post} from "../util/network";
import {AxiosResponse} from "axios";
import {Response} from "./Common";
import {PostItem, MapItem} from "../types"
import {PreviewCardProps} from '../components/PreviewCard';

type CategoryListResponse = Array<{
        idx: number,
        name: string
    }>
export const categoryList = (): Promise<AxiosResponse<CategoryListResponse>> => {
    return get({
      path: '/post/categoryList'
    })
}

type AddCategoryResponse = Response &{
	idx?: number
}
export const addCategory = (data: {name: string}): Promise<AxiosResponse<AddCategoryResponse>> => {
	return get({
		path: '/post/addCategory',
		data: data
	})
}

export const modCategory = (data: {prev: string, next: string}): Promise<AxiosResponse<Response>> => {
	return put({
		path: '/post/modCategory',
		data: data
	})
}

export const delCategory = (data: {idx: number}): Promise<AxiosResponse<Response>> => {
	return del({
		path: '/post/delCategory',
		data: data
	})
}

export const getMapInfo = (data: {url: string}): Promise<AxiosResponse<Array<MapItem>>> => {
	return get({
		path: '/post/getMapInfo',
		data: data
	})
}

export const addPost = (data: PostItem): Promise<AxiosResponse<Response>> => {
	return post({
		path: '/post/addPost',
		data: data
	})
}

// 마크저장시 페북에서 태그추출
export type MarkFBResponse = {
	preview: PreviewCardProps | null,
	tags: Array<string>
}
export const getMarkFB = (data: {url: string}): Promise<AxiosResponse<MarkFBResponse>> => {
	return get({
		path: '/post/getMarkFB',
		data: data
	})
}

export type PostFBParam = & PostItem & {
	preview: PreviewCardProps,
	tags: Array<string>
}
export const addPostMarkFB = (data: PostFBParam): Promise<AxiosResponse<Response>> => {
	return post({
		path: '/post/addPostMarkFB',
		data: data
	})
}

export type ViewPostResponse = PostItem & {
	category_name: string,
	date: string,
	expire: string,
	preview: PreviewCardProps | null,
	mark: Array<MapItem>
}

export const getPost = (data: {idx: number}): Promise<AxiosResponse<ViewPostResponse>> => {
	return get({
		path: '/post/getPost',
		data: data
	})
}

export const modPost = (data: PostItem): Promise<AxiosResponse<Response>> => {
	return post({
		path: '/post/modPost',
		data: data
	})
}

export const modPostMarkFB = (data: PostFBParam): Promise<AxiosResponse<Response>> => {
	return post({
		path: '/post/modPostMarkFB',
		data: data
	})
}

export const delPost = (data: {idx: number}): Promise<AxiosResponse<Response>> => {
	return del({
		path: '/post/delPost',
		data: data
	})
}