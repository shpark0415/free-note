import {post} from "../util/network";
import {get} from "../util/network";
import {AxiosResponse} from "axios";
import {Response} from "./Common";

export const join = (data: {id: string, password: string}): Promise<AxiosResponse<Response>> => {
    return post({
      path: '/user/register',
      data: data
    })
}

type LoginResponse = Response & {token?: string}
export const login = (data: {id: string, password: string, maintain: boolean}): Promise<AxiosResponse<LoginResponse>> => {
    return post({
      path: '/user/login',
      data: data
    })
}

export const chkPwd = (data: {id: string, password: string}): Promise<AxiosResponse<Response>> => {
  return post({
    path: '/user/chkPwd',
    data: data
  })
}

// 리덕스에 저장할 유저정보 반환
type RefreshResponse = {result: boolean, id?: string}
export const refresh = (): Promise<AxiosResponse<RefreshResponse>> => {
  return get({
    path: '/user/refresh',
  })
}