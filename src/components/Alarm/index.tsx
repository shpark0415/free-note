import React from 'react';
import {History} from 'history';
import Carousel from "react-multi-carousel";
import CustomDots from '../../components/CustomDots';
import CloseIcon from '@material-ui/icons/Close';
import {VIEW} from "../../routes/Links";
import Style from "./style.module.scss";

export type AlarmItem = {
    id: number,
    title?: string,
    description?: string,
    date: string,
    dday: number,
    lock: number
}

type AlarmProps = {
    history: History,
    list: Array<AlarmItem>,
    onClose: (e: boolean) => void;
}

Alarm.defaultProps = {
    title: '',
    description: '',
    onClose: ()=>{},
    lock: 0
};

export default function Alarm({history, ...props}: AlarmProps) {

    const showDday = (dday: number) => {
        if(dday===0) return "D-Day!"
        return "D-" + dday;
    }

    const content = (list: AlarmItem) => {
        let show = [];

        if(list.lock > 0) return <h3>비밀 글입니다.</h3>;
        else {
            if(list.title) show.push(<h3 key={'t'+list.id}>{list.title}</h3>);
            if(list.description) show.push(<div key={'d'+list.id} className={Style.description}>{list.description}</div>);
        }
        return show;
    }

    return (
        <div className={Style.alarmWrap}>                
            <div className={Style.closeBtn}>
                <CloseIcon color={'inherit'} fontSize={'inherit'}
                        onClick={()=>props.onClose(false)}/>                    
            </div>
            <p className={Style.description}>*곧 기한이 만료됩니다.</p>
            <Carousel
                additionalTransfrom={0}
                arrows={false}
                showDots customDot={<CustomDots/>}
                autoPlay={true}
                autoPlaySpeed={8500}
                customTransition="all 1s"
                transitionDuration={1000}
                centerMode={false}
                focusOnSelect={false}
                infinite
                responsive={{
                    desktop: {
                        breakpoint: {
                            max: 1920,
                            min: 769
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    },
                    mobile: {
                        breakpoint: {
                            max: 768,
                            min: 0
                        },
                        items: 1,
                        partialVisibilityGutter: 30
                    }
                }}
            >
                {props.list.map((list: AlarmItem)=>
                    <div key={'al'+list.id} className={Style.content} onClick={()=>history.push(VIEW+'/'+list.id)}>
                        {content(list)}
                        <div className={Style.date}>
                            <strong className={Style.dday}>{showDday(list.dday)}</strong>
                            <span className={Style.expire}>{list.date}</span>
                        </div>
                    </div>  
                )}
            </Carousel>
        </div>
    )
};