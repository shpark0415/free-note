import * as React from 'react'
import {Component} from 'react';
import Style from "./style.module.scss";
const classNames = require('classnames');

type ButtonProps = {
    value: string,
    color: 'blue' | 'skyblue' | 'pink' | 'apricot',
    onClick: ()=>void;
};

type ButtonState = {};

class Button extends Component<ButtonProps, ButtonState> {

    state = {}

    static defaultProps: ButtonProps = {
        value: '',
        color: 'blue',
        onClick: ()=>{}
    }

    componentDidMount(): void {}

    render() {
        return <div className={classNames({
            [Style.container]: true,
            [Style['color-' + this.props.color]]: true
        })} onClick={this.props.onClick}>
            {this.props.value}
        </div>
    };
}

export default Button;