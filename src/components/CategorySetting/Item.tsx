import * as React from 'react'
import {Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {paramEvent} from '../../types';
import Style from './style.module.scss';
const classNames = require('classnames');

type ItemProps = {
    value: paramEvent,
    onChange: (i: paramEvent)=>void,
    clickState: paramEvent | null,      //현재 클릭된 아이템
    remove: (i: paramEvent)=>void,
    edit: (prev:paramEvent, next:string)=>void,
    rollBack: (i:paramEvent)=>void,
};

type ItemState = {
    click: boolean,
    editMode: boolean,
    editLabel: string
}

class Item extends Component<ItemProps, ItemState> {

    state: ItemState = {
        click: false,
        editMode: false,     // 편집모드
        editLabel: this.props.value?.label
    }

    static getDerivedStateFromProps(nextProps: ItemProps, prevState: ItemState){
        if(nextProps.value?.value === nextProps.clickState?.value) {
            // 동일한 이름으로 수정했을 때 다시수정할때 예전 이름으로 rollback
            if(prevState.editMode===false && nextProps.value.label!==prevState.editLabel) {
                return {
                    click: true,
                    editLabel: nextProps.value.label
                }
            }
            else return {click: true};
        }
        else return {
            click: false, 
            editMode: false,
            editLabel: nextProps.value?.label
        };
    } 

    static defaultProps = {
        value: null,
        onChange: ()=>{},
        clickState: null,
        remove: ()=>{},
        edit: ()=>{},
        rollBack: ()=>{}
    }

    handleClick = () => {       // 라인클릭
        this.setState({click: true})
        this.props.onChange(this.props.value)
    }

    handleEdit = () => {        // 수정 아이콘
        this.setState({editMode: !this.state.editMode})
        if(this.props.value?.label !== this.state.editLabel)
            this.props.edit(this.props.value, this.state.editLabel);    // prev, next    
    }

    handleRemove = () => {      // 삭제 아이콘
        if(window.confirm("삭제하시겠습니까?")){
            this.setState({click: false})
            this.props.remove(this.props.value)
        } else return;        
    }

    editLine = () => {
        return <input type='text' value={this.state.editLabel} 
                    className={Style.editLine} onChange={(e)=>this.setState({editLabel: e.target.value})}/>
    }

    render() {
        return <li onClick={()=>this.handleClick()}
                    className={classNames({
                        [Style.categoryList]: true,
                        [Style.active]: this.state.click
                    })}>
            {this.state.editMode ? this.editLine() : this.props.value?.label}
            {this.state.click && <div className={Style.iconContainer}>
                <EditIcon color={'inherit'} fontSize={'inherit'} onClick={this.handleEdit}/> &nbsp;
                <DeleteIcon color={'inherit'} fontSize={'inherit'} onClick={this.handleRemove}/>
            </div>}
        </li>
    };
}

export default Item;