import * as React from 'react'
import {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {StoreState} from "../../store/modules";
import {PostInfo, CategoryInfo, actionCreators} from "../../store/modules/Post";
import AddIcon from '@material-ui/icons/Add';
import * as PostAPI from "../../api/Post";
import {checkError} from '../../util/error';
import Modal from '../Modal';
import Separator from '../Separator';
import Item from './Item';
import {paramEvent} from '../../types';
import Style from './style.module.scss';

type CategorySettingProps = {
    post: PostInfo,
    postActions: {
        [key: string]: Function
    },

    modalClose: ()=>void
};

type CategorySettingState = {
    clickItem: paramEvent | null,
    addItem: string
}

// props에서 value를 받아오고 state에 넣었지만, props대신 리덕스를 사용할 것...
class CategorySetting extends Component<CategorySettingProps, CategorySettingState> {

    state: CategorySettingState = {
        clickItem: null,
        addItem: ''
    }

    static defaultProps = {
        modalClose: ()=>{}
    }

    handleRemove = (i: paramEvent) => {        
        //api호출
        PostAPI.delCategory({idx: i.value}).then(res => {
            if(res.data.result){
                let tempValue: Array<paramEvent> = this.props.post.category;
                tempValue.splice(tempValue.findIndex((item)=>item.value===i.value), 1);
                this.props.postActions.addCategory(tempValue)
            }
        }).catch(checkError)
    }

    // params에 value, label값이 다르게 옴
    handleEdit = (prevData: paramEvent, next: string) => {
        let tempValue: Array<paramEvent> = this.props.post.category;
        const idx = tempValue.findIndex((item)=>item.label===prevData.label);  // query updqte에 필요한 index 추출

        if(tempValue.find((element)=>{  // 동일한 이름 검사
            if(element.label===next) return true;
        })) {
                alert("동일한 이름의 카테고리가 존재합니다.")
                return;
        } else {
            //api호출
            PostAPI.modCategory({prev: prevData.label, next: next}).then(res => {                
                tempValue[idx] = {
                    value: prevData.value,
                    label: next
                };
                this.props.postActions.addCategory(tempValue)
            }).catch(checkError)
        }
    }

    menuList = () => {
        return this.props.post.category.map((i:paramEvent, idx:number) => 
            <Item key={idx} value={i} clickState={this.state.clickItem} 
                onChange={(i: paramEvent)=>this.setState({clickItem: i})}
                remove={(i: paramEvent)=>this.handleRemove(i)}
                edit={(prev, next)=>this.handleEdit(prev,next)} />);
    }

    addCategory = () => {
        if(!this.state.addItem) {
            alert("카테고리 이름을 입력해 주세요.")
            return;
        }

        const category = this.props.post.category

        if(category.find((element)=>{
            if(element.label===this.state.addItem) return true;
        })) {
            alert("동일한 이름의 카테고리가 존재합니다.")
            return;
        } else {
            //api호출
            PostAPI.addCategory({name: this.state.addItem}).then(res => {
                const result = res.data;
                
                category.push({
                    value: result.idx as number,
                    label: this.state.addItem
                })
                this.props.postActions.addCategory(category)
                this.setState({addItem: ''})

            }).catch(checkError)
        }
    }

    render() {
        return <Modal width={80} height={300} maxHeight={300} modalClose={this.props.modalClose}>
            <div className={Style.container}>
                <h3 className={Style.title}>카테고리 설정</h3>
                <Separator/>
                <ul style={{marginTop: '-10px'}}>
                    <li className={Style.addLine}>
                        <input type='text' value={this.state.addItem} 
                            onChange={(e)=>this.setState({addItem: e.target.value})} />
                        <div style={{cursor: 'pointer'}}><AddIcon color={'inherit'} onClick={this.addCategory}/></div>
                    </li>
                    {this.menuList()}
                </ul>
            </div>
        </Modal>
    };
}

export default connect(
    ({post}: StoreState) => ({post: post}),
    (dispatch) => ({
        postActions: bindActionCreators(actionCreators, dispatch),
    })
)(CategorySetting);