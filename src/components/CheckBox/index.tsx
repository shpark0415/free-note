import React, {useEffect, useState} from 'react';
import CheckIcon from '@material-ui/icons/Check';
import Style from './style.module.scss';
const classNames = require('classnames');

type CheckBoxProps = {
    value: string,
    defaultChecked: boolean,
    onChange: (e: boolean) => void
}

CheckBox.defaultProps = {
    value: '',
    defaultChecked: false,
    onChange: () => {},
};

export default function CheckBox(props: CheckBoxProps) {
    const [checked, setChecked] = useState<boolean>(props.defaultChecked);

    useEffect(()=> {
        setChecked(props.defaultChecked);
    }, [props.defaultChecked])

    const handleClick = () => {
        setChecked(!checked)
        props.onChange(!checked)
    }

    
    return ( <>
        <div className={Style.chkWrap} onClick={e=> handleClick()}>
            <div className={classNames({
                [Style.checkBox]: true,
                [Style.checked]: checked

            })}>
                <div className={classNames({
                    [Style.checkIcon]: true,
                    [Style.checked]: checked
                })}>
                    <CheckIcon color={'inherit'} fontSize={'inherit'}/>
                </div>
            </div>
            {props.value && <p className={Style.label}>{props.value}</p>}
        </div>
    </>)
};