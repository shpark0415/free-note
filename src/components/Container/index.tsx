import * as React from 'react';
import Style from "./style.module.scss";

type ContainerProps = {
    children: React.ReactNode
    height: string
}

Container.defaultProps = {
    height: 'auto'
};

export default function Container({children, height}: ContainerProps) {
    return (
        <div style={{height: height}} className={Style.container}>
            {children}
        </div>
    )
}
