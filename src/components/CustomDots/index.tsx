import React from 'react'
import Style from './style.module.scss';
const classNames = require('classnames');

type DotType = {
    active?: boolean,
    onClick?: any
    index?: number
}

const CustomDots = (props: DotType) => {

    return (
        <button className={classNames({
            [Style.button]: true,
            [Style.active]: props.active
        })}
                onClick={e => props.onClick(e)}
        />
    )
};

export default CustomDots;
