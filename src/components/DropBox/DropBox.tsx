import React, {useEffect, useState} from 'react';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Modal from '../Modal';
import Item from './Item';
import {paramEvent} from './index';
import Style from './style.module.scss';

type DropBoxProps = {
    list: Array<paramEvent>,
    value: paramEvent,
    onChange: (e: paramEvent)=>void
}

DropBox.defaultProps = {
    list: [],
    value: null,
    onChange: ()=>{}
};

export default function DropBox(props: DropBoxProps) {
    const [open, setOpen] = useState<boolean>(false);

    useEffect(()=> {
        // dropbox 리스트가 변경되거나 삭제될 때
        const item = chkValue();
        if(item) handleChange(item as paramEvent);
        else handleChange({value: 0, label: '카테고리를 선택해주세요.'})
    }, [JSON.stringify(props.list)])

    const chkValue = () => {
        return props.list.find(i=>i.value===props.value?.value);
    }

    const getLabel = () => {
        const item = chkValue();

        if(item) return item.label;
        else return '카테고리를 선택해주세요.';
    }

    const handleChange = (e: paramEvent) => {
        props.onChange(e)
    }

    return ( <>
        <div className={Style.container} onClick={()=>setOpen(true)}>
            <p>{getLabel()}</p>
            <div className={Style.iconBox}>
                <ArrowDropDownIcon color={'inherit'}/>
            </div>
        </div>
        {open && <Modal width={120} maxHeight={300} modalClose={()=>setOpen(false)}>
            {props.list.map((e:paramEvent, idx:number)=>
                <Item key={e.value} item={e} 
                        active={props.value?.value===e.value} 
                        onChange={(e:paramEvent)=>handleChange(e)}
                /> 
            )}
        </Modal>}
    </>)
};