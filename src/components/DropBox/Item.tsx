import React, {Component} from 'react';
import Separator from '../Separator';
import {paramEvent} from './index';
import Style from './style.module.scss';
const classNames = require('classnames');

type ItemProps = {
    item: paramEvent,
    active: boolean,
    onChange: (e: paramEvent)=>void;
}

class Item extends Component<ItemProps> {

    static defaultProps = {
        item: null,
        active: false,
        onChange: ()=>{}
    }

    handleClick = () => {
        this.props.onChange(this.props.item)
    }

    render() {      
        return <>            
            <div className={Style.list} onClick={()=>this.handleClick()}>
                <p>{this.props.item?.label}</p>
                <div className={Style.circle}>
                    <div className={classNames({
                        [Style.active]: this.props.active,
                    })}/>
                </div>
            </div>                
            <div className={Style.hr}><Separator/></div> 
        </>
    }
}

export default Item;