import * as React from 'react'
import {Component} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actionCreators} from "../../store/modules/User";
import Style from './style.module.scss';
import MenuIcon from '@material-ui/icons/Menu';
import MapIcon from '@material-ui/icons/Map';
import CategoryIcon from '@material-ui/icons/Category';
import StorageIcon from '@material-ui/icons/Storage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {HOMEPAGE, LOGIN, JOIN, POST, MARKER, STORAGE} from '../../routes/Links';

const classNames = require('classnames');

type HeaderProps = RouteComponentProps & {
    userActions: {
        [key: string]: Function
    }
};

type HeaderState = {
    open: boolean;
};

export type MenuProps = {
    value: string;
    link: string;
    icon: JSX.Element;
}

export const menu: Array<MenuProps> = [
    {
        value: '마커뷰',
        link: MARKER,
        icon: <MapIcon/>
    },
    // {
    //     value: '카테고리',
    //     link: '/category',
    //     icon: <CategoryIcon/>
    // },
    {
        value: '보관함',
        link: STORAGE,
        icon: <StorageIcon/>
    },
    {
        value: '로그아웃',
        link: '',
        icon: <ExitToAppIcon/>
    }
]

class Header extends Component<HeaderProps, HeaderState> {

    state: HeaderState = {
        open: false
    }

    noHeader = [LOGIN, JOIN, POST];

    isNoHeader(pathname: string){
        return this.noHeader.some(path => pathname.indexOf(path) === 0);
    };

    componentDidMount(): void {}

    liClick = (value: string) => {
        this.setState({open: !this.state.open})
        if(value === '로그아웃') {
            this.props.userActions.logout();
        }
    }

    render() {
        const pathname = this.props.history.location.pathname;

        return <>
            {!this.isNoHeader(pathname) && 
                <div className={Style.container}>
                    <Link to={HOMEPAGE}>
                        <img onClick={e=>this.setState({open: false})} src={'/assets/images/logo.png'} alt='logo'/>
                    </Link>
                    <div className={classNames({
                        [Style.menuBox]: true,
                        [Style.open]: this.state.open
                    })} 
                        onClick={e=>this.setState({open: !this.state.open})}>
                        <MenuIcon color={'inherit'} />
                    </div>
                    <div className={classNames({
                        [Style.navContainer]: true,
                        [Style.open]: this.state.open
                    })}>
                        <nav className={classNames({
                            [Style.menu]: true,
                            [Style.open]: this.state.open
                        })}>
                            <ul>
                                {menu.map((li)=>
                                    <Link key={li.value} to={li.link}>
                                        <li onClick={()=>this.liClick(li.value)}>                                            
                                            {li.icon}
                                            <span>{li.value}</span>
                                        </li>
                                    </Link>
                                )}
                            </ul>
                        </nav>
                    </div>
                </div>
            }
        </>
    };
}

export default withRouter(connect(
    undefined,
    (dispatch) => ({
        userActions: bindActionCreators(actionCreators, dispatch),
    })
)(Header));