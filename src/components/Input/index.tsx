import React, {useState, useEffect} from 'react';
import Style from './style.module.scss';
const classNames = require('classnames');

type InputProps = {
    onChange: (e: string)=>void,
    defaultValue: string,
    placeholder: string,
    width?: string,
    className: string,
    type?: string
}

Input.defaultProps = {
    onChange: ()=>{},
    defaultValue: '',
    placeholder: '',
    width: '100%',
    className: '',
    type: 'input'
};

export default function Input(props: InputProps) {
    const [value, setValue] = useState<string>(props.defaultValue);

    useEffect(()=> {
        setValue(props.defaultValue)
    }, [props.defaultValue])

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value)
        props.onChange(e.target.value)
    }

    return ( <>
        <div className={Style.container} style={{width: props.width}}>
            <input className={classNames({
                [Style.inputBox]: true,
                [props.className]: true
            })}
                value={value} placeholder={props.placeholder}
                onChange={e=>handleChange(e)} type={props.type}/>
        </div>
    </>)
};