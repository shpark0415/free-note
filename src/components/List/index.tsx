import * as React from 'react'
import {Component} from 'react';
import PreviewCard, {PreviewCardProps} from '../PreviewCard';
import Star from '../Star';
import Tag from '../Tag';
import Separator from '../Separator';
import CheckBox from '../CheckBox';
import LockIcon from '@material-ui/icons/Lock';
import Style from './style.module.scss';

type ListProps = {
    id: number,
    title?: string,
    description?: string,
    url?: string,
    date: string,
    star: number,
    confirm?: boolean,
    secret: boolean,
    isSelect: boolean,
    onSelect: (e: boolean)=>void,
    onClick: ()=>void,
    preview?: PreviewCardProps,
};

class List extends Component<ListProps> {

    static defaultProps = {
        title: '',
        description: '',
        url: '',
        date: '',
        star: 0,
        confirm: false,
        secret: false,
        isSelect: false,
        onSelect: null,
        onClick: ()=>{},
        preview: {},
    }

    contentRender = () => {
        let content = []
        if(!this.props.secret){
            content.push(<div style={{cursor: 'pointer'}} key={this.props.id} onClick={this.props.onClick}>
                {this.props.title && <div className={Style.title}>{this.props.title}</div>}
                {this.props.description && <div className={Style.description}>{this.props.description}</div>}
            </div>)
            
            if(this.props.preview?.post_idx !== undefined)
                content.push(<div style={{cursor: 'pointer'}} key={'prev'+this.props.preview?.post_idx} className={Style.previewBox}>
                        <PreviewCard {...this.props.preview}/>
                    </div>);
        } else {
            content.push(<div style={{cursor: 'pointer'}} key={this.props.id} onClick={this.props.onClick}>
                {this.props.title && <div className={Style.title} style={{marginBottom: '8px'}}>{this.props.title}</div>}
                <div className={Style.secretBox}>
                    <LockIcon color={'inherit'}/>
                </div>
            </div>)
        }
        return content;
    }

    render() {
        return <div className={Style.container}>
            <Separator/>  
            {this.props.onSelect &&
                <div className={Style.checkBoxWrap}>
                    <CheckBox defaultChecked={this.props.isSelect} onChange={e=>this.props.onSelect(e)} />
                </div>
            }   
            <div className={Style.headWrap} onClick={this.props.onClick}>
                <div className={Style.left}>
                    <p className={Style.date}>{this.props.date}</p>
                    {this.props.star > 0 && <Star count={this.props.star}/>}
                </div>
                {this.props.confirm && <Tag value={'완료'} color={'blue'} />}
            </div>    
            <div style={{clear: 'both'}}/>
            {this.contentRender()}
        </div>
    };
}

export default List;