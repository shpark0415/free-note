import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {StoreState} from "../../store/modules";
import {PostInfo, actionCreators} from "../../store/modules/Post";
import PhotoSizeSelectLargeIcon from '@material-ui/icons/PhotoSizeSelectLarge';
import PhotoSizeSelectSmallIcon from '@material-ui/icons/PhotoSizeSelectSmall';
import Style from './style.module.scss';

type ListHeaderProps = {
    // redux
    post: PostInfo,
    postActions: {
        [key: string]: Function
    },

    allCnt: number
}

class ListHeader extends Component<ListHeaderProps> {

    static defaultProps = {
        allCnt: 0
    }

    renderIcon = () => {
        if(this.props.post?.view === 'list')
            return <PhotoSizeSelectLargeIcon color={'inherit'} fontSize={'inherit'} 
                        onClick={()=>{this.props.postActions.changeViewMode('album')}}/>;
        else if(this.props.post?.view == 'album')
            return <PhotoSizeSelectSmallIcon color={'inherit'} fontSize={'inherit'} 
                        onClick={()=>{this.props.postActions.changeViewMode('list')}}/>;
    }

    render() {
        return <div className={Style.headWrap}>
            <p className={Style.count}>{this.props.allCnt}건</p>
            <div className={Style.viewMode}>
                {this.renderIcon()}
            </div>
        </div>
    }
}

export default connect(
    ({post}: StoreState) => ({post: post}),
    (dispatch) => ({
        postActions: bindActionCreators(actionCreators, dispatch),
    })
)(ListHeader);