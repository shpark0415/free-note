import React from 'react';
import Style from "./style.module.scss";

const classNames = require('classnames');

type LoadingPageProps = {
    backgroundColor: 'white' | 'inner'
}

LoadingPage.defaultProps = {
    backgroundColor: 'white'
};

export default function LoadingPage(props: LoadingPageProps) {

    return (
        <div className={classNames({
            [Style.wrap]: true,
            [Style['bgColor-'+props.backgroundColor]]: true
        })}>
            <svg className={Style.spinner} viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                <circle className={Style.path} fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30"/>
            </svg>
        </div>
    )
};