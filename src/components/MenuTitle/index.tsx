import * as React from 'react'
import { Component } from 'react';
import { RouteComponentProps, withRouter } from "react-router";
import { menu, MenuProps } from '../../components/Header';
import Style from './style.module.scss';
const classNames = require('classnames');

type MenuTitleProps = RouteComponentProps & {
    pink?: boolean;
};

class MenuTitle extends Component<MenuTitleProps> {

    static defaultProps = {
        pink: false
    }

    render() {
        let path = this.props.match.path;
        const title = menu.filter((li: MenuProps) => li.link === path);

        return <div className={classNames({
            [Style.titleWrap]: true,
            [Style.pink]: this.props.pink
        })}>
            <div className={Style.icon}>{title[0].icon}</div>
            <h2 className={Style.title}>{title[0].value}</h2>
        </div>
    };
}

export default withRouter(MenuTitle);