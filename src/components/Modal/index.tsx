import React from 'react';
import Style from "./style.module.scss";

type ModalProps = {
    children: React.ReactNode,
    modalClose: () => void,   //X 클릭,
    width?: number,
    height: number,
    maxHeight?: number
}

Modal.defaultProps = {
    modalClose: () => {},
    width: 60,
    height: -1,
    maxHeight: 10000
};

export default function Modal(props: ModalProps) {

    let width = 'calc(100% - ' +props.width+ 'px)';
    let height = props.height === -1 ? 'auto' : props.height;
    let maxHeight = props.maxHeight + 'px';

    return (
        <div className={Style.container}>
            <div className={Style.background} onClick={e=>props.modalClose()}/>    {/* 바깥영역 클릭시 닫기.. */}
            <div className={Style.contentBox} style={{width: width, height: height, maxHeight: maxHeight}}>
                {props.children}
            </div>
        </div>
    )
};