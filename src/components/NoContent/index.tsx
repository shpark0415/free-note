import React from 'react';
import Style from "./style.module.scss";

export default function NoContent() {
    return (
        <div className={Style.container}>
            조회된 데이터가 없습니다.
        </div>
    )
};