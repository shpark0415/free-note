import * as React from 'react'
import {Component} from 'react';
import {connect} from "react-redux";
import {PostInfo} from "../../store/modules/Post";
import {StoreState} from "../../store/modules";
import Style from './style.module.scss';

const classNames = require('classnames');

export type PreviewCardProps = {
    // redux
    post?: PostInfo,

    // props
    post_idx?: number,
    title?: string,
    content?: string,
    filePath?: string, 
    url?: string
};

type PreviewCardState = {};

class PreviewCard extends Component<PreviewCardProps, PreviewCardState> {

    static defaultProps = {
        post_idx: 0,
        title: '',
        content: '',
        filePath: '',
        url: '',
    }

    render() {
        return <div className={Style.container}>
            <a className={classNames({
                [Style.wrap]: true,
                [Style['view-' + this.props.post?.view]]: true
            })} href={this.props.url} target={"_blank"}>
                {this.props.filePath && <img className={classNames({
                    [Style['imgBox-' + this.props.post?.view]]: true,
                })} src={this.props.filePath} alt='preview' />}
                <div className={Style.contentBox}>
                    <p className={Style.title}>{this.props.title}</p>
                    <p className={Style.description}>{this.props.content}</p>
                </div>
            </a>
        </div>
    };
}

export default connect(
    ({post}: StoreState) => ({post: post})
)(PreviewCard)