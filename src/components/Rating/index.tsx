import React, {Component} from 'react';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import Style from './style.module.scss';
// const classNames = require('classnames');

type RatingProps = {
    max: number,
    value: number,
    onChange: (n: number)=>void
}

type RatingState = {
    click: number,
}

class Rating extends Component<RatingProps, RatingState> {

    state: RatingState = {
        click: this.props.value
    }

    static defaultProps: RatingProps = {
        max: 5,
        value: 0,
        onChange: ()=>{}
    }

    componentDidMount(): void {}

    fillStar = (i: number) => {
        if(this.props.value===0) return <StarBorderIcon color={'inherit'}/>;
        else if(i<=this.props.value) return <StarIcon color={'inherit'}/>;
        else return <StarBorderIcon color={'inherit'}/>;
    }

    handleClick = (i: number) => {  // 별 클릭했을 때
        if(this.state.click === i) {
            this.setState({click: 0})
            this.props.onChange(0)
        } else {
            this.setState({click: i})
            this.props.onChange(i)
        }  
    }

    starRender = () => {
        let starComponent = []
        for(let i=1; i<=this.props.max; i++) 
            starComponent.push(<div key={i} className={Style.starContainer} onClick={()=>this.handleClick(i)} onMouseOver={()=>this.props.onChange(i)} >
                {this.fillStar(i)}
            </div>);
        return starComponent;
    }

    render() {
        return <div className={Style.container} onMouseOut={()=>this.props.onChange(this.state.click)}>
            {this.starRender()}
        </div>
    }
}

export default Rating;