import * as React from 'react'
import {Component} from 'react';
import {District} from '../../types';
import SearchIcon from '@material-ui/icons/Search';
import Input from '../Input';
import Style from './style.module.scss';

type SearchProps = {
    placehodler?: string,
    onChange: (e:string)=>void,
    list: Array<District>,
    onClick: (e:string)=>void;
};

type SearchState = {
    open: boolean,
    value: string
}

class Search extends Component<SearchProps, SearchState> {

    static defaultProps: SearchProps = {
        placehodler: '검색어를 입력하세요.',
        onChange: ()=>{},
        list: [],
        onClick: ()=>{}
    }

    state: SearchState = {
        open: false,
        value: ''
    }

    content = () => {
        if(this.state.open) {
            if(this.props.list.length > 0) {
                return <ul className={Style.list}>
                    {this.props.list.map(li => 
                        <li key={li.district} className={Style.item} 
                            onClick={()=>this.selectItem(li.district)}>
                            {li.district}
                        </li>
                    )}
                </ul>
            }
        }
    }

    selectItem = (li: string) => {
        this.setState({
            open: false,
            value: li
        })
        this.props.onClick(li)
    }

    render() {
        return <div className={Style.searchWrap}>
            <div className={Style.container} onClick={()=>this.setState({open: true})}>
                <Input width={'calc(100% - 50px)'} onChange={(e:string)=>this.props.onChange(e)} 
                    placeholder={this.props.placehodler} className={Style.customInput} 
                    defaultValue={this.state.value}/>
                <div className={Style.iconWrap}>
                    <div className={Style.iconBox}>
                        <SearchIcon color={'inherit'}/>
                    </div>
                </div>
            </div>
            {this.content()}
        </div>
    };
}

export default Search;