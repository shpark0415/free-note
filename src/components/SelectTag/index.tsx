import * as React from 'react'
import {Component} from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Modal from '../Modal';
import CheckBox from '../CheckBox';
import Style from './style.module.scss';

export type SelectTagProps = {
    list: Array<string>,
    confirm: (list: Array<string>)=>void;
};

type SelectTagState = {
    store: Array<string>
};

class SelectTag extends Component<SelectTagProps, SelectTagState> {

    static defaultProps: SelectTagProps = {
        list: [],
        confirm: ()=>{}
    }

    state: SelectTagState = {
        store: []
    }

    push = (e: boolean, item: string) => {
        let temp = this.state.store
        let idx = temp.findIndex(i=>i === item)

        if(e) {     // 추가
            if(idx === -1) temp.push(item);
        } else {    // 삭제
            temp.splice(idx, 1)
        }

        this.setState({store: temp})
    }

    render() {
        return <Modal width={80} maxHeight={400}>
            <div className={Style.openMark}>
                <div className={Style.title}>
                    <span className={Style.icon}>
                        <FavoriteIcon color={'inherit'} fontSize={'inherit'}/>
                    </span>
                    <p>마크에 표시할 장소를 선택해주세요.</p>
                </div>
                {this.props.list.map((item,idx) =>
                    <div key={'mark'+idx} className={Style.content}>
                        <CheckBox onChange={(e)=>this.push(e, item)}/>
                        <div className={Style.item}>
                            <p>{item}</p>
                        </div>
                    </div>
                )}
                <div className={Style.btn} 
                    onClick={()=>this.props.confirm(this.state.store)}>
                    저장
                </div>
            </div>
        </Modal>
    };
}

export default SelectTag;