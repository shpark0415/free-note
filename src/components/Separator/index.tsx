import React from 'react';
import Style from "./style.module.scss";

type SeparatorProps = {
    
}

Separator.defaultProps = {
    
};

export default function Separator(props: SeparatorProps) {
    return (
        <hr className={Style.separator}/>
    )
};