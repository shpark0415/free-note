import * as React from 'react'
import {Component} from 'react';
import StarIcon from '@material-ui/icons/Star';
import Style from './style.module.scss';

type StarProps = {
    count: number,
};

class Star extends Component<StarProps> {

    static defaultProps: StarProps = {
        count: 0
    }

    getStar = () => {
        let tempStar = []
        for(let i=0; i<this.props.count; i++) tempStar.push(<StarIcon key={i} fontSize={'inherit'} color={'inherit'}/>);
        return tempStar;
    }

    render() {
        return <div className={Style.starBox}>
            {this.getStar()}
        </div>
    };
}

export default Star;