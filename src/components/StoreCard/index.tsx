import * as React from 'react'
import {Component} from 'react';
import Style from './style.module.scss';

export type StoreCardProps = {
    title?: string,
    content?: string,
    filePath?: string, 
    url?: string
};

type StoreCardState = {};

class StoreCard extends Component<StoreCardProps, StoreCardState> {

    static defaultProps: StoreCardProps = {
        title: '',
        content: '',
        filePath: '',
        url: ''
    }

    render() {
        return <a href={this.props.url} target={"_blank"}>
            <div className={Style.container}>
                <div className={Style.contentBox}>
                    <p className={Style.title}>{this.props.title}</p>
                    <p className={Style.description}>{this.props.content}</p>
                    <span className={Style.link}>가게정보 상세보기 &gt;</span>
                </div>
                {this.props.filePath && 
                    <img src={this.props.filePath} alt='preview' className={Style.imgBox} />
                }
            </div>
        </a>
    };
}

export default StoreCard;