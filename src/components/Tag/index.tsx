import * as React from 'react'
import {Component} from 'react';
import Style from './style.module.scss';
const classNames = require('classnames');

type TagProps = {
    value: string,
    color: 'blue' | 'pink'
};

class Tag extends Component<TagProps> {

    static defaultProps: TagProps = {
        value: '',
        color: 'blue'
    }

    render() {
        return <div className={classNames({
            [Style.container]: true,
            [Style[this.props.color]]: true
        })}>
            {this.props.value}
        </div>
    };
}

export default Tag;