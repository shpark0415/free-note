import React, {useEffect, useState} from 'react';
import Style from './style.module.scss';
const classNames = require('classnames');

type WideCheckBoxProps = {
    label: string,
    checked: boolean,
    onChange: (e: boolean) => void
};

WideCheckBox.defaultProps = {
    label: '',
    onChange: () => {},
    checked: false
}

export default function WideCheckBox (props: WideCheckBoxProps) {
    const [checked, setChecked] = useState<boolean>(props.checked);

    useEffect(()=> {
        setChecked(props.checked)
    }, [props.checked])

    const handleChange = () => {
        setChecked(true)
        props.onChange(true)
    }

    return ( <>
        <div className={classNames({
            [Style.container]: true,
            [Style.checked]: checked

            })} onClick={e=> handleChange()}>
                <p>{props.label}</p>
        </div>
    </>)
}