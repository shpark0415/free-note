import * as React from 'react'
import {Link} from "react-router-dom";
import {Component} from 'react';
import CreateIcon from '@material-ui/icons/Create';
import Style from './style.module.scss';
import {POST} from "../../routes/Links";
const classNames = require('classnames');

type WriteFABProps = {};

type WriteFABState = {};

class WriteFAB extends Component<WriteFABProps, WriteFABState> {

    static defaultProps: WriteFABProps = {

    }

    render() {
        return <div className={Style.container}>
            <Link to={POST}>
                <div className={Style.iconBox}>
                    <CreateIcon color={'inherit'}/>
                </div>
            </Link>
        </div>
    };
}

export default WriteFAB;