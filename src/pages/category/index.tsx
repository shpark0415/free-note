import * as React from 'react'
import {Component} from 'react';
import Container from '../../components/Container';
import WriteFAB from '../../components/WriteFAB';
import MenuTitle from '../../components/MenuTitle';
import DropBox, {paramEvent} from '../../components/DropBox';
import List from '../../components/List';
import Style from './style.module.scss';

type CategoryProps = {};

type CategoryState = {};

class Category extends Component<CategoryProps, CategoryState> {

    state: CategoryState = {}

    componentDidMount(): void {}

    render() {
        let tempValue: Array<paramEvent> = [
            {value: 1, label: '맛집'},
            {value: 2, label: '스터디'},
            {value: 3, label: '게임'},
            {value: 4, label: '기타'}
        ]

        return <Container>
            <MenuTitle />
            <div className={Style.headWrap}>
                <div className={Style.dropBox}>
                    {/* DropBox의 value를 state값으로바꿔야 선택할때 움직임 */}
                    <DropBox onChange={(e:paramEvent)=>console.log(e)} 
                            list={tempValue} value={tempValue[2]} />
                </div>
                <p>57건</p>
            </div>
            <div>
                <List id={1} onClick={()=>console.log('list click')} title={'WAN의 PPP 프로토콜'} description={'WAN : 내가 직접 네트워크 케이블을 깔아서 통신을 연결할 수 없을때 사용하는 네트워킹 방식 WAN : 내가 직접 네트워크 케이블을 깔아서'} 
                        url={'https://blog.naver.com/jjypink81/222054967729'} date={'2020.5.23.'} star={3} confirm/>
                <List id={2} onClick={()=>console.log('list click')} title={'WAN의 PPP 프로토콜'} description={'WAN : 내가 직접 네트워크 케이블을 깔아서 통신을 연결할 수 없을때 사용하는 네트워킹 방식 WAN : 내가 직접 네트워크 케이블을 깔아서'} 
                        date={'2020.5.23.'} star={1} confirm/>
                <List id={3} onClick={()=>console.log('list click')} title={'WAN의 PPP 프로토콜'} description={'WAN : 내가 직접 네트워크 케이블을 깔아서 통신을 연결할 수 없을때 사용하는 네트워킹 방식 WAN : 내가 직접 네트워크 케이블을 깔아서'} 
                        date={'2020.5.23.'}/>
                <List id={4} onClick={()=>console.log('list click')} title={'WAN의 PPP 프로토콜'} description={'WAN : 내가 직접 네트워크 케이블을 깔아서 통신을 연결할 수 없을때 사용하는 네트워킹 방식 WAN : 내가 직접 네트워크 케이블을 깔아서'} 
                        url={'https://blog.naver.com/jjypink81/222054967729'} star={2} date={'2020.5.23.'} secret/>
            </div>
            <WriteFAB />
        </Container>
    };
}

export default Category;