import React, {Component} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {StoreState} from "../../store/modules";
import {LoginInfo} from "../../store/modules/User";
import {PostInfo, CategoryInfo, actionCreators} from "../../store/modules/Post";
import {VIEW} from '../../routes/Links';
import * as ListAPI from "../../api/List";
import * as UserAPI from "../../api/User";
import * as PostAPI from "../../api/Post";
import {listType} from "../../api/List";
import {checkError} from '../../util/error';
import Modal from '../../components/Modal';
import ListHeader from '../../components/ListHeader';
import NoContent from '../../components/NoContent';
import Container from '../../components/Container';
import LoadingPage from '../../components/LoadingPage';
import WriteFAB from '../../components/WriteFAB';
import Search from '../../components/Search';
import DropBox, {paramEvent} from '../../components/DropBox';
import Input from '../../components/Input';
import Button from '../../components/Button';
import List from '../../components/List';
import Alarm, {AlarmItem} from "../../components/Alarm";
import FilterListIcon from '@material-ui/icons/FilterList';
import {yyyymmdd2, chkExpire, dateSort} from '../../util/date';
import Style from './style.module.scss';
import "react-multi-carousel/lib/styles.css";

type MainProps = RouteComponentProps & {
    // redux
    user: LoginInfo,
    post: PostInfo,
    postActions: {
        [key: string]: Function
    }
}

type MainState = {
    alarmBox: boolean,
    searchValue: string,
    categoryValue: paramEvent,
    list: Array<listType>,
    allCnt: number,
    alarm: Array<AlarmItem>,
    page: number,
    endPage: boolean,
    lockModal: number,  // 클릭한 포스트의 index
    lockPwd: string,
    categoryList: CategoryInfo,
    loading: boolean
}

class Main extends Component<MainProps, MainState> {

    state: MainState = {
        alarmBox: false,
        searchValue: '',
        categoryValue: {value: 0, label: '전체'},
        list: [],
        allCnt: 0,
        alarm: [],
        page: 0,
        endPage: false,
        lockModal: -1,
        lockPwd: '',
        categoryList: [{value: 0, label: '전체'}],
        loading: false
    }

    componentDidMount(): void {
        this.getAlarmLists()
        this.getCategoryList()  // 카테고리 리스트 부르기
        this.getPostLists()
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    }

    // 스크롤 제일 아래로 내렸을때
    onScroll = (e: any) => {
        let scrollTop = document.documentElement.scrollTop;
        let windowHeight = document.documentElement.clientHeight;
        let scrollHeight = document.documentElement.scrollHeight;

        if((scrollTop + windowHeight) >= scrollHeight-500) {
            window.removeEventListener('scroll', this.onScroll); 

            this.setState({
                page: this.state.page + 1
            }, ()=>this.getPostLists())    
                   
        }
    }

    getAlarmLists = () => {
        ListAPI.getAlarm().then(res => {
            const result = res.data;
            this.setState({
                alarm: dateSort(result),
                alarmBox: result.length > 0 ? true : false,
            })
        }).catch(checkError)
    }

    getCategoryList = () => {
        // 전체 추가
        PostAPI.categoryList().then(res => {
            const result = res.data;

            const category: CategoryInfo = []
            result.map(li=>{
                category.push({
                    value: li.idx,
                    label: li.name
                })
            })

            let tempCategoryList: CategoryInfo = JSON.parse(JSON.stringify(category))
            let tempStateCategoryList = this.state.categoryList
            this.setState({categoryList: tempStateCategoryList.concat(tempCategoryList)})
            console.log(tempStateCategoryList.concat(tempCategoryList))

            this.props.postActions.addCategory(category)        
        }).catch(checkError)
    }

    getPostLists = () => {
        this.setState({loading: true})

        ListAPI.getList({
            page: this.state.page,
            keyword: this.state.searchValue,
            category_idx: this.state.categoryValue.value
        }).then(res => {
            const result = res.data;
            let tempList: Array<listType> = this.state.list

            this.setState({
                list: tempList.concat(result.list),
                allCnt: result.allCnt,       
                endPage: result.endPage,
                loading: false
            })
            if(!result.endPage) window.addEventListener('scroll', this.onScroll);

        }).catch((res)=>{
            window.removeEventListener('scroll', this.onScroll); 
            checkError(res)
        })
    }

    selectLists = (e: paramEvent) => {
        const stateValue = this.state.categoryValue.value
        this.setState({
            categoryValue: e,
        }, ()=>{
            if(stateValue !== e.value) {
                this.setState({
                    page: 0,
                    list: [],
                    allCnt: 0
                }, ()=> this.getPostLists())
            }
        })
    }

    searchLists = (e: string) => {
        this.setState({
            searchValue: e,
            page: 0,
            list: [],
            allCnt: 0
        }, ()=> this.getPostLists())
    }

    listClick = (item: listType) => {
        if(item.lock) this.setState({lockModal: item.idx as number});
        else this.props.history.push(VIEW+'/'+item.idx);
    }

    content = () => {
        if(this.state.allCnt > 0) {
            return this.state.list.map((item: listType)=>
                <List key={item.idx as number}
                    id={item.idx as number} 
                    onClick={()=>this.listClick(item)} 
                    title={item.title} 
                    description={item.content} 
                    url={item.url} 
                    date={yyyymmdd2(item.date as string)} 
                    star={item.star} 
                    confirm={chkExpire(item.expire as string)}
                    secret={item.lock>0 ? true : false}
                    preview={item.preview} />
            )
        } else {
            if(this.state.loading) return <LoadingPage backgroundColor='inner'/>;
            else return <NoContent />;
        }
    }

    getContainerHeight = () => {
        if(this.state.loading === false ) {
            if(this.state.allCnt > 0) return 'auto';
            else return this.state.alarm.length>0 ? 'calc(100% - 110px - 150px)' : 'calc(100% - 110px)';
        }
        else return this.state.alarm.length>0 ? 'calc(100% - 110px - 150px)' : 'calc(100% - 110px)';
    }
    
    lockConfirm = () => {
        UserAPI.chkPwd({id: this.props.user.id, password: this.state.lockPwd}).then(res => {
            this.props.history.push(VIEW+'/'+this.state.lockModal);
        }).catch(checkError)
    }

    render() {
        return <>
        {this.state.alarmBox && 
            <Alarm history={this.props.history} list={this.state.alarm} onClose={(e)=>this.setState({alarmBox: e})} />}

        <Container height={this.getContainerHeight()}>
            <Search onChange={(e:string)=>this.searchLists(e)}/>
            <div className={Style.filterContainer}>
                <div className={Style.iconBox}>
                    <FilterListIcon color={'inherit'}/>
                </div>
                <DropBox onChange={(e:paramEvent)=>this.selectLists(e)} 
                        list={this.state.categoryList} 
                        value={this.state.categoryValue} />
            </div>
            <div className={Style.contentContainer}>
                <ListHeader allCnt={this.state.allCnt}/>
                <div className={Style.contentWrap}>
                    {this.content()}
                    {(this.state.loading && this.state.allCnt > 0) &&
                        <div className={Style.loading}><LoadingPage backgroundColor='inner'/></div>}
                </div>
            </div>
            <WriteFAB />
        </Container>

        {this.state.lockModal > -1 && <Modal modalClose={()=>this.setState({lockModal: -1})}>
                <Input type={'password'} onChange={e=>this.setState({lockPwd: e})} placeholder={'비밀번호를 입력하세요.'}/>
                <Button value={'확인'} onClick={()=>this.lockConfirm()} color='skyblue'/>
            </Modal>}
        </>
    }
}

export default withRouter(connect(
    ({user, post}: StoreState) => ({user: user, post: post}),
    (dispatch) => ({
        postActions: bindActionCreators(actionCreators, dispatch),
    })
)(Main));