import * as React from 'react'
import { Component } from 'react';
import { Kakao, District, MapItem } from '../../types';
import * as Map from '../../api/Map';
import { getPost, ViewPostResponse } from '../../api/Post';
import MenuTitle from '../../components/MenuTitle';
import Search from '../../components/Search';
import StoreCard from '../../components/StoreCard';
import PreviewCard from '../../components/PreviewCard';
import Tag from '../../components/Tag';
import Star from '../../components/Star';
import { checkError } from '../../util/error';
import { yyyymmdd2, chkExpire } from '../../util/date';
import { parseEnter } from '../../util/string';
import Style from './style.module.scss';
const classNames = require('classnames');

type MarkerProps = {};

type MarkerState = ViewPostResponse & {
    hideContent: boolean,
    contentOverlay: boolean,
    keyword: string,
    positions: Array<Map.MarksResponse>,    // 마커를 표시할 위치와 title 객체 배열
    districts: Array<District>,
};

declare let window: Kakao;

class Marker extends Component<MarkerProps, MarkerState> {

    state: MarkerState = {
        hideContent: true,
        contentOverlay: false,
        keyword: '',
        positions: [],
        districts: [],

        // post item
        idx: 0,
        category_name: '',
        title: '',
        url: '',
        content: '',
        star: 0,
        lock: 0,
        date: '',
        expire: '',
        preview: null,
        mark: []
    }

    map = null;     // 지도 element

    componentDidMount() {
        this.getMarks()
    }

    getMarks = () => {
        Map.getMarks().then(res => {
            const result = res.data;
            this.setState({ positions: result }, () => this.showMap())
        }).catch(checkError)
    }

    getDistrict = (e: string) => {
        this.setState({ keyword: e }, () => {
            Map.getDistrict({ keyword: e }).then(res => {
                const result = res.data;
                this.setState({ districts: result })
            }).catch(checkError)
        })
    }

    selectDistrict = (e: string) => {
        this.setState({
            keyword: e,     // 검색키워드 입력
            districts: []   // 리스트 초기화
        }, () => this.moveLatLng(this.map))
    }

    // 해당지역으로 이동
    moveLatLng = (map: any) => {
        // 주소-좌표 변환 객체를 생성합니다
        var geocoder = new window.kakao.maps.services.Geocoder();

        // @ts-ignore
        // 주소로 좌표를 검색합니다
        geocoder.addressSearch(this.state.keyword, function (result, status) {
            if (status === window.kakao.maps.services.Status.OK) {  // 정상적으로 검색이 완료됐으면 
                var moveLatLon = new window.kakao.maps.LatLng(result[0].y, result[0].x);
                map.panTo(moveLatLon);  // 지도 중심을 이동 시킵니다
            }
        });
    }

    showMap = () => {
        Map.getCoordinates().then(res => {
            const result = res.data;

            const container = document.getElementById('map'); // 지도를 표시할 div 
            const options = { //지도를 생성할 때 필요한 기본 옵션
                center: new window.kakao.maps.LatLng(result.latitude, result.longitude), //지도의 중심좌표
                level: result.zoom //지도의 확대 레벨
            };

            this.map = new window.kakao.maps.Map(container, options); // 지도를 생성합니다

            const imageSrc = '/assets/images/map_icon.png' // 마커이미지의 주소입니다 
            const imageSize = new window.kakao.maps.Size(22, 29); // 마커 이미지의 이미지 크기 입니다
            const imageOption = { offset: new window.kakao.maps.Point(8, 27) }; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
            const markerImage = new window.kakao.maps.MarkerImage(imageSrc, imageSize, imageOption); // 마커 이미지를 생성합니다 

            for (let i = 0; i < this.state.positions.length; i++) {
                const marker = new window.kakao.maps.Marker({    // 마커를 생성합니다
                    map: this.map,
                    position: new window.kakao.maps.LatLng(this.state.positions[i].latitude, this.state.positions[i].longitude), // 마커를 표시할 위치
                    title: this.state.positions[i].title, // 마커의 타이틀
                    image: markerImage // 마커 이미지 
                });

                // 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
                const content = this.addElement(this.state.positions[i]);
                const position = new window.kakao.maps.LatLng(this.state.positions[i].latitude, this.state.positions[i].longitude); // 커스텀 오버레이가 표시될 위치입니다 
                const customOverlay = new window.kakao.maps.CustomOverlay({  // 커스텀 오버레이를 생성합니다
                    map: this.map,
                    position: position,
                    content: content,
                    yAnchor: 1
                });
            }

            // 마우스 드래그로 지도 이동이 완료
            window.kakao.maps.event.addListener(this.map, 'dragend', () => this.mapMoved(this.map));
            // 지도가 확대 또는 축소
            window.kakao.maps.event.addListener(this.map, 'zoom_changed', () => this.mapMoved(this.map))

        }).catch(checkError)
    }

    mapMoved = (map: any) => {
        const latlng = map.getCenter(); // 지도 중심좌표를 얻어옵니다   
        const level = map.getLevel();   // 지도의 현재 레벨을 얻어옵니다
        Map.setCoordinates({
            latitude: latlng.getLat(),
            longitude: latlng.getLng(),
            zoom: level
        }).catch(checkError)
    }

    addElement = (item: Map.MarksResponse) => {
        var customoverlay = document.createElement("div");
        customoverlay.setAttribute("id", "customoverlay");

        const div = document.createElement("div");
        const span = document.createElement("span");
        const newContent = document.createTextNode(item.title);
        span.appendChild(newContent);
        div.appendChild(span);
        customoverlay.appendChild(div);

        customoverlay.classList.add(Style.customoverlay)
        customoverlay.addEventListener("click", this.onClickMarker(item, this));

        return customoverlay;
    }

    onClickMarker = (item: Map.MarksResponse, me: any) => {
        return function (e: any) {
            e.stopPropagation();    // 이벤트버블링

            getPost({ idx: item.post_idx }).then(res => {
                const result = res.data;
                let tempMark: Array<MapItem> = result.mark;
                result.mark.length > 1 && result.mark.map((li, idx) => {
                    if ((li.title as string).includes(item.title)) {
                        tempMark.splice(idx, 1)
                        tempMark.unshift(li)
                        return;
                    }
                })
                me.setState({
                    ...result,
                    mark: tempMark,
                    hideContent: false,  // 마커클릭하면 컨텐트 보이게
                })
            }).catch(checkError)
        };
    }

    render() {
        return <div className={Style.wrap}>
            <div className={Style.menuTitle}><MenuTitle pink /></div>
            <div className={Style.container}>
                <div className={Style.searchWrap}>
                    <Search placehodler={'지역명을 입력하세요.'}
                        onChange={(e: string) => this.getDistrict(e)}
                        list={this.state.districts}
                        onClick={(e: string) => this.selectDistrict(e)}
                    />
                </div>
                <div id="map" className={Style.mapWrap} onClick={() => this.setState({ hideContent: true, contentOverlay: false })} />
                <div className={classNames({
                    [Style.contentWrap]: true,
                    [Style.hide]: this.state.hideContent,
                    [Style.overlay]: this.state.contentOverlay
                })} >
                    <div className={Style.top} onClick={(e) => this.setState({ contentOverlay: !this.state.contentOverlay })} >
                        <div className={Style.separator} />
                    </div>
                    <div className={Style.contentBox}>
                        {this.state.mark.map(item =>
                            <div className={Style.storeCardWrap} key={'card' + item.idx}>
                                <StoreCard {...item} />
                            </div>
                        )}
                        <hr className={Style.separator} />

                        <div className={Style.category}>
                            {chkExpire(this.state.expire as string) && <><Tag value={'완료'} color={'blue'} />&nbsp;&nbsp;</>}
                            {this.state.expire && yyyymmdd2(this.state.expire) + ' / '}
                            {this.state.category_name}
                            {this.state.star > 0 && <>&nbsp;<Star count={this.state.star} /></>}
                        </div>
                        {this.state.title && <h2 className={Style.title}>{this.state.title}</h2>}
                        {this.state.content && <div className={Style.content} dangerouslySetInnerHTML={{ __html: parseEnter(this.state.content) }} />}
                        {this.state.preview && <div className={Style.previewWrap}>
                            <PreviewCard {...this.state.preview} />
                        </div>}

                        <div className={Style.endLine}>{yyyymmdd2(this.state.date)}</div>
                    </div>
                </div>
            </div>
        </div>
    };
}

export default Marker;