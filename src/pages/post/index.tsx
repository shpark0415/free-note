import React, {Component} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import * as PostAPI from "../../api/Post";
import {checkError} from '../../util/error';
import {connect} from "react-redux";
import {StoreState} from "../../store/modules";
import {PostInfo} from "../../store/modules/Post";
import {HOMEPAGE} from "../../routes/Links";
import {PostItem, MapItem} from '../../types';
import Calendar from 'react-calendar';
import CloseIcon from '@material-ui/icons/Close';
import DoneIcon from '@material-ui/icons/Done';
import SettingsIcon from '@material-ui/icons/Settings';
import {yyyymmdd, toMysqlFormat} from '../../util/date';
import CategorySetting from '../../components/CategorySetting';
import Modal from '../../components/Modal';
import Rating from '../../components/Rating';
import Separator from '../../components/Separator';
import CheckBox from '../../components/CheckBox';
import {PreviewCardProps} from '../../components/PreviewCard';
import DropBox, {paramEvent} from '../../components/DropBox';
import SelectMark from '../../components/SelectMark';
import SelectTag from '../../components/SelectTag';
import 'react-calendar/dist/Calendar.css';
import Style from './style.module.scss';
const classNames = require('classnames');

type PostProps = RouteComponentProps & {
    // redux
    post: PostInfo
}

type PostState = {
    idx: number,
    title: string,
    url: string,
    paramUrl?: string,
    mark: boolean,                     // mark 저장여부
    openMark: Array<MapItem>,          // mark modal open (크롤링리스트)
    openTag: PostAPI.MarkFBResponse,          // tag modal open (태그크롤링리스트)
    content: string,
    openMenuSetting: boolean,   // 카테고리 메뉴 open/close
    categoryValue: paramEvent,  // 카테고리 선택 값
    category_idx: number,       // 카테고리 param key
    star: number,               // 중요도
    useLock: boolean,           // 잠금설정
    useExpire: boolean,         // 기한설정
    expire: Date,           
    openCalendar: boolean,
    request: boolean            // 저장중
}

class Post extends Component<PostProps, PostState> {

    state:PostState = {
        idx: 0,
        mark: false,
        openMark: [],
        openTag: {
            preview: null,
            tags: []
        },
        openMenuSetting: false,
        useExpire: false,
        openCalendar: false,
        request: false,

        title: '',
        url: '',
        paramUrl: '',
        content: '',
        category_idx: 0,
        categoryValue: {value: 0, label: '카테고리를 선택해주세요.'},
        star: 0,
        useLock: false,     // lock
        expire: new Date()  // new Date(this.props.expire)
    }

    componentDidMount(): void {
        if(this.props.history.location.state) {
            this.setState({
                ...this.state,
                ...this.props.history.location.state as PostState
            })
        }
        
        this.getCategoryValue() // 카테고리 초기값 설정
    }

    componentDidUpdate() {
        // 컴포넌트 처음 마운트될 때 카테고리 defaultValue 적용하기 위함
        if(this.state.categoryValue.value===0) this.getCategoryValue();
    }

    getCategoryValue = () => {
        const item = this.props.post.category.find(i=>i.value===this.state.category_idx);
        if(item) this.setState({categoryValue: item});
    }

    // 저장
    handleDone = () => {
        if(this.state.request) return;

        if(this.state.categoryValue.value === 0){
            alert('카테고리를 선택해 주세요.')
            return;
        }

        this.setState({request: true})

        if(this.state.mark && this.state.url!=='') {     // 마크저장 check on

            if(this.state.url.indexOf('facebook.com') !== -1) {     // 페이스북 전용 API
                PostAPI.getMarkFB({url: this.state.url}).then(res => {
                    let result: PostAPI.MarkFBResponse = res.data;
                    if(result.tags.length===0) this.addPost([]);
                    else this.setState({openTag: result});
                }).catch(checkError)
            }
            else {
                let result: Array<MapItem> = [];
                PostAPI.getMapInfo({url: this.state.url}).then(res => {
                    result = res.data;
                    // map이 여러개일 경우
                    if(result.length > 1) this.setState({openMark: result})
                    // map이 하나일 경우
                    else this.addPost(result)
                }).catch(checkError)
            }
        }

        else this.addPost([])
    }

    addPost = (mark: Array<MapItem>) => {
        let param: PostItem = {
            title: this.state.title,             
            url: this.state.url,               
            content: this.state.content,         
            category_idx: this.state.categoryValue.value, 
            star: this.state.star,       
            lock: this.state.useLock ? 1 : 0, 
            expire: toMysqlFormat(this.state.expire),
            useExpire: this.state.useExpire,
            mark: mark
        }
        PostAPI.addPost(param).then(res => {
            this.props.history.push(HOMEPAGE)
        }).catch(checkError).finally(()=>this.setState({request: false}))
    }

    // post + mark 저장
    addPostMarkFB = (tags: Array<string>) => {
        let param: PostAPI.PostFBParam = {
            title: this.state.title,             
            url: this.state.url,               
            content: this.state.content,         
            category_idx: this.state.categoryValue.value, 
            star: this.state.star,       
            lock: this.state.useLock ? 1 : 0, 
            expire: toMysqlFormat(this.state.expire),
            useExpire: this.state.useExpire,
            preview: this.state.openTag.preview as PreviewCardProps,
            tags: tags
        }
        PostAPI.addPostMarkFB(param).then(res => {
            this.props.history.push(HOMEPAGE)
        }).catch(checkError).finally(()=>this.setState({request: false}))
    }

    // 업데이트
    handleUpdate = () => {
        if(this.state.request) return;

        this.setState({request: true})

        if((this.state.url !== this.state.paramUrl) && this.state.url!=='') {
            if(this.state.mark) {     // 마크저장 check on
                if(this.state.url.indexOf('facebook.com') !== -1) {     // 페이스북 전용 API
                    PostAPI.getMarkFB({url: this.state.url}).then(res => {
                        let result: PostAPI.MarkFBResponse = res.data;
                        if(result.tags.length===0) this.modPost([]);
                        else this.setState({openTag: result});
                    }).catch(checkError)
                }
                else {
                    let result: Array<MapItem> = [];
                    PostAPI.getMapInfo({url: this.state.url}).then(res => {
                        result = res.data;
                        // map이 여러개일 경우
                        if(result.length > 1) this.setState({openMark: result})
                        // map이 하나일 경우
                        else this.modPost(result)
                    }).catch(checkError)
                }                
            }
            else this.modPost([])
        }
        else this.modPost([])
    }

    modPost = (mark: Array<MapItem>) => {
        let param: PostItem = {
            idx: this.state.idx,
            title: this.state.title,             
            url: this.state.url,               
            content: this.state.content,         
            category_idx: this.state.categoryValue.value, 
            star: this.state.star,       
            lock: this.state.useLock ? 1 : 0, 
            expire: toMysqlFormat(this.state.expire),
            useExpire: this.state.useExpire,
            mark: mark,
            urlUpdate: this.state.url !== this.state.paramUrl
        }
        PostAPI.modPost(param).then(res => {
            this.props.history.push(HOMEPAGE)
        }).catch(checkError).finally(()=>this.setState({request: false}))
    }

    modPostMarkFB = (tags: Array<string>) => {
        let param: PostAPI.PostFBParam = {
            idx: this.state.idx,
            title: this.state.title,             
            url: this.state.url,               
            content: this.state.content,         
            category_idx: this.state.categoryValue.value, 
            star: this.state.star,       
            lock: this.state.useLock ? 1 : 0, 
            expire: toMysqlFormat(this.state.expire),
            useExpire: this.state.useExpire,
            urlUpdate: this.state.url !== this.state.paramUrl,            
            preview: this.state.openTag.preview as PreviewCardProps,
            tags: tags
        }
        PostAPI.modPostMarkFB(param).then(res => {
            this.props.history.push(HOMEPAGE)
        }).catch(checkError).finally(()=>this.setState({request: false}))
    }

    handleUseExpire = (e: boolean) => {
        this.setState({
            useExpire: e,
            openCalendar: e
        })
    }

    render() {
        return <>
            <div className={Style.postHeader}>
                <div style={{cursor: 'pointer'}}><CloseIcon color={'inherit'} onClick={()=>this.props.history.goBack()}/></div>
                <div style={{cursor: 'pointer'}}><DoneIcon color={'inherit'} onClick={()=>this.state.idx===0 ? this.handleDone() : this.handleUpdate()}/></div>
            </div>
            <div className={Style.postContent}>
                <div className={Style.line}>
                    <input type='text' value={this.state.title} onChange={e=>this.setState({title: e.target.value})} placeholder='제목' className={Style.title}/>
                </div>
                <Separator/>
                <div className={Style.line}>
                    <input type='text' value={this.state.url} onChange={e=>this.setState({url: e.target.value})} placeholder='url을 입력하세요.' className={Style.url}/>
                    <div className={Style.markBox}>
                        <CheckBox value={'마크저장'} defaultChecked={this.state.mark} onChange={e=>{this.setState({mark: e})}} />
                    </div>
                </div>
                <Separator/>
                <div className={classNames({
                    [Style.line]: true,
                    [Style.contentHeight]: true
                })}>
                    <textarea placeholder='내용을 입력하세요.' value={this.state.content} onChange={e=>this.setState({content: e.target.value})} className={Style.content}/>
                </div>
                <Separator/>
                <div className={Style.line}>
                    <p className={Style.subTitle}>카테고리</p>
                    <div className={Style.subContent}>
                        <DropBox onChange={(e:paramEvent)=>this.setState({categoryValue: e})} list={this.props.post.category} value={this.state.categoryValue}/>
                        <div className={Style.categorySetting} onClick={()=>this.setState({openMenuSetting: true})}>
                            <SettingsIcon color={'inherit'}/>
                        </div>
                    </div>
                </div>
                <Separator/>
                <div className={Style.line}>
                    <p className={Style.subTitle}>북마크</p>
                    <div className={Style.subContent}>
                        <Rating max={3} value={this.state.star} onChange={(n: number)=>this.setState({star: n})}/>
                    </div>
                </div>
                <Separator/>
                <div className={Style.line}>
                    <p className={Style.subTitle}>잠금설정</p>
                    <div className={Style.subContent}>
                        <div style={{marginBottom: '-15px'}}>
                            <CheckBox onChange={(e: boolean)=>this.setState({useLock: e})} defaultChecked={this.state.useLock}/>
                        </div>
                    </div>
                </div>
                <Separator/>
                <div className={Style.line}>
                    <p className={Style.subTitle}>기한설정</p>
                    <div className={Style.subContent}>
                        {this.state.useExpire &&
                            <p className={Style.date} onClick={()=>this.setState({openCalendar: true})}>
                            {yyyymmdd(this.state.expire)} &nbsp;
                        </p>}
                        <div style={{marginBottom: '-15px'}}>
                            <CheckBox defaultChecked={this.state.useExpire} onChange={this.handleUseExpire} />
                        </div>
                    </div>
                </div>
            </div>

            {this.state.openMenuSetting && <CategorySetting modalClose={()=>this.setState({openMenuSetting: false})} />}

            {this.state.openCalendar && <Modal width={80} modalClose={()=>this.setState({openCalendar: false})}>
                <Calendar
                    onChange={(date: any)=>this.setState({expire: date})}
                    value={this.state.expire}
                    className={Style.customCalendar}
                />
            </Modal>}

            {this.state.openMark.length>0 &&
                <SelectMark list={this.state.openMark} 
                            confirm={(list)=>this.state.idx===0 ? this.addPost(list) : this.modPost(list)}
                />
            }

            {this.state.openTag.preview &&
                <SelectTag list={this.state.openTag.tags} 
                            confirm={(list)=>this.state.idx===0 ? this.addPostMarkFB(list): this.modPostMarkFB(list)}
                />
            }
        </>
    }
}

export default withRouter(connect(
    ({post}: StoreState) => ({post: post})
)(Post));