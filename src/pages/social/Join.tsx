import React, {ChangeEvent, useState} from 'react';
import {Link} from "react-router-dom";
import {RouteComponentProps} from "react-router";
import Input from '../../components/Input';
import Button from '../../components/Button';
import Style from "./style.module.scss";
import {HOMEPAGE, LOGIN} from "../../routes/Links";
import * as UserAPI from "../../api/User";
import {EMAIL_REGEXP} from "../../util/string";

type JoinProps = RouteComponentProps & {}

Join.defaultProps = {};

export default function Join({history}: JoinProps) {
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [rePassword, setRePassword] = useState<string>("");

    const _handleJoin = () => {
        if(!email) {
            alert('아이디를 입력해 주세요.')
            return;
        } else if(!EMAIL_REGEXP.test(email)) {
            alert('이메일 형식을 맞춰 주세요.')
            return;
        } else if(!password || !rePassword) {
            alert('비밀번호를 입력해 주세요.')
            return;
        } else if(password.length<6) {
            alert('비밀번호는 6자리 이상 입력해주세요.')
            return;
        } else if(password !== rePassword) {
            alert('비밀번호를 다시 확인해주세요.')
            return;
        }

        UserAPI.join({
            id: email,
            password: password,
        }).then(res => {
            const result = res.data;

            if(result.result === true) {
                alert(result.message)
                history.push(LOGIN)
            } else if(result.result === false) {
                alert(result.message)
                return;
            }
        }).catch((res) => {
            alert(res.response.data.message)
        })
    }

    return (
        <div className={Style.backgroundWrap}>
            <div className={Style.container}>
                <Link to={HOMEPAGE}><img src={'/assets/images/logo.png'} alt='logo'/></Link>
                <div className={Style.formBox}>
                    <Input onChange={(e:string)=>setEmail(e)} placeholder={'아이디를 입력하세요.'}/>
                    <Input onChange={(e:string)=>setPassword(e)} placeholder={'비밀번호를 입력하세요.'} type={'password'}/>
                    <Input onChange={(e:string)=>setRePassword(e)} placeholder={'비밀번호를 확인합니다.'} type={'password'}/>
                    <Button onClick={_handleJoin} value={'회원가입'} color={'apricot'}/>
                </div>
                <div className={Style.bottomMsg}>
                    <Link to={LOGIN}>로그인</Link>
                </div>
                
            </div>
        </div>
    )
};