import React, {ChangeEvent, useState} from 'react';
import {RouteComponentProps} from "react-router";
import {Link} from "react-router-dom";
import Input from '../../components/Input';
import Button from '../../components/Button';
import CheckBox from '../../components/CheckBox';
import Style from "./style.module.scss";
import {HOMEPAGE, JOIN} from "../../routes/Links";
import {checkError} from '../../util/error';
import * as UserAPI from "../../api/User";
import {useDispatch} from "react-redux";
import {actionCreators} from "../../store/modules/User";

type LoginProps = RouteComponentProps & {}

Login.defaultProps = {};

export default function Login({history, ...props}: LoginProps) {
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [maintain, setMaintain] = useState<boolean>(false);

    const dispatch = useDispatch();

    const _handleLogin = () => {
        if(!email) {
            alert('아이디를 입력해 주세요.')
            return;
        } else if(!password) {
            alert('비밀번호를 입력해 주세요.')
            return;
        } else if(password.length<6) {
            alert('비밀번호는 6자리 이상 입력해주세요.')
            return;
        }

        UserAPI.login({
            id: email,
            password: password,
            maintain: maintain
        }).then(res => {
            const result = res.data;
            
            localStorage.setItem("token", result.token as string);

            dispatch(actionCreators.login({
                id: email
            }))
            history.push(HOMEPAGE)
        }).catch(checkError)
    }

    return (
        <div className={Style.backgroundWrap}>
            <div className={Style.container}>
                <Link to={HOMEPAGE}><img src={'/assets/images/logo.png'} alt='logo'/></Link>
                <div className={Style.formBox}>
                    <Input onChange={(e:string)=>setEmail(e)} placeholder={'아이디를 입력하세요.'}/>
                    <Input onChange={(e:string)=>setPassword(e)} placeholder={'비밀번호를 입력하세요.'} type={'password'}/>
                    <CheckBox value={'자동 로그인'} onChange={e=>setMaintain(e)} />
                    <Button onClick={_handleLogin} value={'로그인'} color={'skyblue'}/>
                </div>
                <div className={Style.bottomMsg}>
                    <Link to={JOIN}>회원가입</Link>
                </div>
                
            </div>
        </div>
    )
};