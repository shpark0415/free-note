import * as React from 'react'
import {Component} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import {VIEW} from '../../routes/Links';
import {connect} from "react-redux";
import {StoreState} from "../../store/modules";
import {LoginInfo} from "../../store/modules/User";
import Container from '../../components/Container';
import MenuTitle from '../../components/MenuTitle';
import List from '../../components/List';
import CheckBox from '../../components/CheckBox';
import WideCheckBox from '../../components/WideCheckBox';
import NoContent from '../../components/NoContent';
import LoadingPage from '../../components/LoadingPage';
import ListHeader from '../../components/ListHeader';
import Modal from '../../components/Modal';
import Input from '../../components/Input';
import Button from '../../components/Button';
import * as ListAPI from "../../api/List";
import {chkPwd} from "../../api/User";
import {checkError} from '../../util/error';
import {yyyymmdd2, chkExpire} from '../../util/date';
import Style from './style.module.scss';
const classNames = require('classnames');

type StorageProps = RouteComponentProps & {
    // redux
    user: LoginInfo,
};

type StorageState = {
    // radio 
    flagStar: boolean,
    flagExpire: boolean,

    allSelect: boolean,
    list: Array<ListAPI.StorageList>,
    page: number,
    allCnt: number,
    endPage: boolean,
    loading: boolean,
    lockModal: number,  // 클릭한 포스트의 index
    lockPwd: string,
};

class Storage extends Component<StorageProps, StorageState> {

    state: StorageState = {
        flagStar: true,
        flagExpire: false,
        allSelect: false,
        list: [],
        page: 0,
        allCnt: 0,
        endPage: false,
        loading: false,
        lockModal: -1,
        lockPwd: ''
    }

    componentDidMount(): void {
        this.getStarLists()
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    }

    getContainerHeight = () => {
        if(this.state.loading === false ) {
            if(this.state.list.length > 0) return 'auto';
            else return 'calc(100% - 110px)';
        }
        else return 'calc(100% - 110px)';
    }

    getStarLists = () => {
        this.setState({loading: true})

        ListAPI.getStarLists({
            page: this.state.page,
        }).then(res => {
            const result = res.data;
            let tempList: Array<ListAPI.StorageList> = this.state.list

            this.setState({
                list: tempList.concat(result.list),
                allCnt: result.allCnt,
                endPage: result.endPage,
                loading: false
            })
            if(!result.endPage) window.addEventListener('scroll', this.onScroll);

        }).catch((res)=>{
            window.removeEventListener('scroll', this.onScroll); 
            checkError(res)
        })
    }

    getExpireLists = () => {
        this.setState({loading: true})

        ListAPI.getExpireLists({
            page: this.state.page,
        }).then(res => {
            const result = res.data;            
            for(let i=0; i<result.list.length; i++) result.list[i].isSelect = this.state.allSelect ? true : false;

            let tempList: Array<ListAPI.StorageList> = this.state.list
            this.setState({
                list: tempList.concat(result.list),
                allCnt: result.allCnt,
                endPage: result.endPage,
                loading: false
            })
            if(!result.endPage) window.addEventListener('scroll', this.onScroll);

        }).catch((res)=>{
            window.removeEventListener('scroll', this.onScroll); 
            checkError(res)
        })
    }

    // 스크롤 제일 아래로 내렸을때
    onScroll = (e: any) => {
        let scrollTop = document.documentElement.scrollTop;
        let windowHeight = document.documentElement.clientHeight;
        let scrollHeight = document.documentElement.scrollHeight;

        if((scrollTop + windowHeight) >= scrollHeight-500) {
            window.removeEventListener('scroll', this.onScroll); 

            this.setState({
                page: this.state.page + 1
            }, ()=>{
                if(this.state.flagStar) this.getStarLists();
                else if(this.state.flagExpire) this.getExpireLists();
            })    
                   
        }
    }

    handleAllSelect = (e: boolean) => {
        // object in array change
        
        this.setState(prevState => ({
            allSelect: e,
            list: prevState.list.map((li)=>
                { return {...li, isSelect: e} }
            )
        }));  
    }

    handleSelect = (e: boolean, idx: number) => {
        // object in array change
        this.setState(prevState => ({
            list: prevState.list.map(
                li => li.idx === idx? { ...li, isSelect: e } : li
            )
        }));  
    }

    handleDelete = () => {
        let filter = this.state.list.filter(li=>li.isSelect===true);    //삭제대상
        if(filter.length===0) {
            alert('삭제할 게시물을 선택해주세요.');
            return;
        }

        if(window.confirm("삭제하시겠습니까?")){
            // 삭제api
            ListAPI.delExpireLists({list: filter}).then(res => {
                this.setState({
                    page: 0,
                    allCnt: 0,
                    list: [],
                    allSelect: false
                }, ()=>this.getExpireLists())
            }).catch(checkError)
        } else return;
    }

    selectStar = () => {
        if(this.state.flagStar) return;
        this.setState({
            flagStar: true,
            flagExpire: false,
            page: 0,
            allCnt: 0,
            list: [],
            allSelect: false
        }, ()=>this.getStarLists())
    }

    selectExpire = () => {
        if(this.state.flagExpire) return;
        this.setState({
            flagStar: false,
            flagExpire: true,
            page: 0,
            allCnt: 0,
            list: [],
            allSelect: false
        }, ()=>this.getExpireLists())
    }

    listClick = (item: ListAPI.StorageList) => {
        if(item.lock) this.setState({lockModal: item.idx as number});
        else this.props.history.push(VIEW+'/'+item.idx);
    }

    lockConfirm = () => {
        chkPwd({id: this.props.user.id, password: this.state.lockPwd}).then(res => {
            this.props.history.push(VIEW+'/'+this.state.lockModal);
        }).catch(checkError)
    }

    content = () => {       
        if(this.state.list.length > 0) {
            return this.state.list.map((item: ListAPI.StorageList)=> {                
                let flag:any = {}
                if(this.state.flagExpire) {
                    flag['onSelect'] = (e: boolean)=>this.handleSelect(e, item.idx as number)
                    flag['isSelect'] = item.isSelect
                }

                return <List key={item.idx as number}
                    id={item.idx as number} 
                    onClick={()=>this.listClick(item)} 
                    title={item.title} 
                    description={item.content} 
                    url={item.url} 
                    date={yyyymmdd2(item.date as string)} 
                    star={item.star} 
                    confirm={this.state.flagExpire ? true : chkExpire(item.expire as string)}
                    secret={item.lock>0 ? true : false}
                    preview={item.preview} 
                    {...flag}
                />
            })     
        } else {
            if(this.state.loading) return <LoadingPage backgroundColor='inner'/>;
            else return <NoContent />;
        }
    }
    
    render() {
        return <>
            <Container height={this.getContainerHeight()}>
                <MenuTitle/>
                <div className={Style.filter}>
                    <WideCheckBox label={'북마크'} checked={this.state.flagStar} onChange={e=> this.selectStar()}/>
                    <WideCheckBox label={'완료'} checked={this.state.flagExpire} onChange={e=> this.selectExpire()}/>
                </div>
                
                <div className={Style.headWrap}>     
                    {this.state.flagExpire ? <CheckBox value={'전체선택'} onChange={e=>this.handleAllSelect(e)} /> : <div/>} 
                    <div className={Style.btnBox}>
                        {this.state.flagExpire && <div className={Style.delete} onClick={this.handleDelete}>삭제</div>}
                        <ListHeader allCnt={this.state.allCnt}/>
                    </div>              
                </div>
                <div className={classNames({
                    [Style.contentWrap]: true,
                    [Style.wide]: this.state.loading ? (this.state.list.length>0 ? false : true) : (this.state.list.length>0 ? false : true)
                })}>
                    {this.content()}
                    {(this.state.loading && this.state.list.length > 0) &&
                        <div className={Style.loading}><LoadingPage backgroundColor='inner'/></div>}
                </div>
            </Container>

            {this.state.lockModal > -1 && <Modal modalClose={()=>this.setState({lockModal: -1})}>
                <Input type={'password'} onChange={e=>this.setState({lockPwd: e})} placeholder={'비밀번호를 입력하세요.'}/>
                <Button value={'확인'} onClick={()=>this.lockConfirm()} color='skyblue'/>
                </Modal>}
        </>
    };
}
export default withRouter(connect(
    ({user}: StoreState) => ({user: user})
)(Storage));