import * as React from 'react';
import {Component} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import {POST, HOMEPAGE} from '../../routes/Links';
import * as PostAPI from "../../api/Post";
import {yyyymmdd2, chkExpire} from '../../util/date';
import {parseEnter} from '../../util/string';
import {checkError} from '../../util/error';
import Container from '../../components/Container';
import Separator from '../../components/Separator';
import Star from '../../components/Star';
import Tag from '../../components/Tag';
import PreviewCard from '../../components/PreviewCard';
import StoreCard from '../../components/StoreCard';
import StoreIcon from '@material-ui/icons/Store';
import Style from './style.module.scss';
const classNames = require('classnames');

type ViewProps = RouteComponentProps & {};

type ViewState = PostAPI.ViewPostResponse & {
    
};

class View extends Component<ViewProps, ViewState> {

    state: ViewState = {
        idx: 0,
        category_idx: 0,
        category_name: '',
        title: '',
        url: '',
        content: '',
        star: 0,
        lock: 0,
        date: '',
        expire: '',
        preview: null,
        mark: []
    }
    
    componentDidMount(): void {
        this.getPost()
    }

    getPost = () => {
        const idx = parseInt(this.props.location.pathname.split('/view/')[1]);

        PostAPI.getPost({idx: idx}).then(res => {
            const result = res.data;
            this.setState({...result})
        }).catch(checkError)
    }

    delPost = () => {
        const result = window.confirm("글을 삭제하시겠습니까?");
        if(result) {
            const idx = parseInt(this.props.location.pathname.split('/view/')[1]);
            PostAPI.delPost({idx: idx}).then(res => {
                this.props.history.push(HOMEPAGE);
            }).catch(checkError)
        }
        else return;
    }

    render() {

        return <Container>
            <div className={Style.categoryWrap}>
                <div className={Style.category}>
                    {chkExpire(this.state.expire as string) && <><Tag value={'완료'} color={'blue'} />&nbsp;&nbsp;</>}
                    {this.state.expire && yyyymmdd2(this.state.expire)+' / '}
                    {this.state.category_name}
                    {this.state.star >0 && <>&nbsp;<Star count={this.state.star} /></>}
                </div>
                <div className={Style.float}>
                    <span className={classNames({
                        [Style.btn]: true,
                        [Style.delete]: true
                    })} onClick={()=>this.delPost()}>삭제</span>
                    <Link to={{pathname: POST, state: {
                        idx: this.state.idx,
                        title: this.state.title,
                        url: this.state.url,
                        paramUrl: this.state.url,
                        mark: this.state.mark.length>0 ? true : false,
                        content: this.state.content,
                        category_idx: this.state.category_idx,
                        star: this.state.star,
                        useLock: this.state.lock>0 ? true : false,
                        useExpire: this.state.expire ? true : false,
                        expire: this.state.expire ? new Date(this.state.expire) : new Date(),
                    }}}>
                        <span className={Style.btn}>수정</span>
                    </Link>
                </div>
            </div>
            {this.state.title && <h2 className={Style.title}>{this.state.title}</h2>}
            {this.state.content && <div className={Style.content} dangerouslySetInnerHTML={{__html: parseEnter(this.state.content)}} />}
            {this.state.preview && <div className={Style.previewWrap}>
                <PreviewCard {...this.state.preview}/>
            </div>}
            <div className={Style.storeWrap}>
                {this.state.mark.length > 0 && <>
                    <Separator />
                    <div className={Style.section}>
                        <StoreIcon fontSize={'inherit'} color={'inherit'} />
                        <span>PLACE</span>
                    </div>                    
                    {this.state.mark.map(item=> <StoreCard key={'card'+item.idx} {...item} />)}
                </>}
                <div className={Style.endLine}>{yyyymmdd2(this.state.date)}</div>
            </div>
        </Container>
    };
}

export default withRouter(View);