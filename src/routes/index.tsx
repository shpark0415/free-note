import React, {Component, Fragment} from 'react';
import {Redirect, Route, Switch} from 'react-router';
import {BrowserRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {StoreState} from "../store/modules";
import {LoginInfo, actionCreators as userActionCreators} from "../store/modules/User";
import {CategoryInfo, actionCreators as postActionCreators} from "../store/modules/Post";
import * as UserAPI from "../api/User";
import {categoryList} from "../api/Post";
import {checkError} from '../util/error';

import Header from '../components/Header';
import LoadingPage from '../components/LoadingPage';
import Main from '../pages/main';
import Post from '../pages/post';
import Login from '../pages/social/Login';
import Join from '../pages/social/Join';
import Category from '../pages/category';
import Marker from '../pages/marker';
import Storage from '../pages/storage';
import View from '../pages/view';

import { HOMEPAGE, LOGIN, JOIN, POST, CATEGORY, MARKER, STORAGE, VIEW } from './Links';

type RootProps = {
    user: LoginInfo,
    userActions: {
        [key: string]: Function
    },
    postActions: {
        [key: string]: Function
    }
};

type RootState = {
    flag: boolean
};

class Root extends Component<RootProps, RootState> {
    state: RootState = {
        flag: false
    };

    static defaultProps = {};

    componentDidMount() {
        this.refresh()
    }

    // 페이지 이동할 때 마다 store state 저장
    refresh = () => {
        UserAPI.refresh().then(res => {
            const result = res.data;
            if(result.result) {
                this.props.userActions.login({
                    id: result.id
                })

                // 카테고리 리스트 부르기
                categoryList().then(res => {
                    const result = res.data;
        
                    const category: CategoryInfo = []
                    result.map(li=>{
                        category.push({
                            value: li.idx,
                            label: li.name
                        })
                    })
                    this.props.postActions.addCategory(category)        
                }).catch(checkError)
            }
        }).catch((res)=>{
            this.props.userActions.logout();
        }).finally(()=>{
            this.setState({
                flag: true
            })
        })
    }

    render() {
        const isLogin = this.props.user.isLogin;

        return <BrowserRouter>
            {this.state.flag ?
                <Fragment>
                <Header/>
                
                <Switch>
                    <Route path={LOGIN} exact component={Login}/>
                    <Route path={JOIN} exact component={Join}/>
                    {isLogin && <>
                        <Route path={HOMEPAGE} exact component={Main}/>                 
                        <Route path={POST} exact component={Post}/>
                        <Route path={CATEGORY} exact component={Category}/>
                        <Route path={MARKER} exact component={Marker}/>
                        <Route path={STORAGE} exact component={Storage}/>
                        <Route path={VIEW+'/:id'} exact component={View}/>
                    </>}
                    <Redirect path="*" to={isLogin ? HOMEPAGE : LOGIN}/>
                </Switch>
            </Fragment>
            : <Fragment>
                <LoadingPage />
            </Fragment>
            }
        </BrowserRouter>
    };
}

export default connect(
    ({user}: StoreState) => ({user: user}),
    (dispatch) => ({
        userActions: bindActionCreators(userActionCreators, dispatch),
        postActions: bindActionCreators(postActionCreators, dispatch),
    })
)(Root);