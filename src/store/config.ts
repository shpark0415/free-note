import modules, {StoreState} from "./modules";
import {createStore, Store, compose} from "redux";

export default function configureStore(): Store<StoreState> {
    if (!!(window as any).__REDUX_DEVTOOLS_EXTENSION__) {
        return createStore(
            modules,
            compose(
                (window as any).__REDUX_DEVTOOLS_EXTENSION__()
            )
        );
    } else
        return createStore(
            modules
        );
}
