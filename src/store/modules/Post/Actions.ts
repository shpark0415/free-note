import * as types from "./types";

// 액션 생성 함수. 인터페이스로 정의한 타입을 리턴 타입으로 사용
const addCategory = (CategoryInfo: types.CategoryInfo) => {    
    return {
        type: types.ADD_CATEGORY,
        category: CategoryInfo
    }
}

const changeViewMode = (view: types.PreviewMode) => {    
    return {
        type: types.CHANGE_VIEW_MODE,
        view: view
    }
}

export const actionCreators = {
    addCategory,
    changeViewMode,
}