import type {PostInfo, PostActionTypes} from "./types";
import * as types from "./types";

const initialState: PostInfo = {
    category: [],
    view: 'list'
};

export default function userReducer(
    state = initialState,
    action: PostActionTypes
):PostInfo {
    switch (action.type) {
        case types.ADD_CATEGORY:
            return {
                ...state,
                category: action.category
            };
        case types.CHANGE_VIEW_MODE:
            return {
                ...state,
                view: action.view
            };
        default:
            return state;
    }
}
