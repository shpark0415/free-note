export const ADD_CATEGORY = "post/ADD_CATEGORY";
export const CHANGE_VIEW_MODE = "post/CHANGE_VIEW_MODE"

export type CategoryInfo = Array<{
    value: number,
    label: string
}>

export type PostInfo = {
    category: CategoryInfo,
    view: PreviewMode
}

export type PreviewMode = 'list' | 'album';

// 생성자의 반환 타입
export type AddCategoryAction = {
    type: typeof ADD_CATEGORY,
    category: CategoryInfo
}

export type ChangeViewMode = {
    type: typeof CHANGE_VIEW_MODE,
    view: PreviewMode
}

export type PostActionTypes =
    | AddCategoryAction
    | ChangeViewMode;