import * as types from "./types";

// 액션 생성 함수. 인터페이스로 정의한 타입을 리턴 타입으로 사용
const login = (LoginInfo: types.LoginInfo) => {    
    return {
        type: types.LOGIN,
        id: LoginInfo.id,
        isLogin: true,
    }
}

const logout = () => {
    localStorage.removeItem("token")
    return {
        type: types.LOGOUT,
    }
}

export const actionCreators = {
    login,
    logout
}