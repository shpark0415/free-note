import type {LoginInfo, UserActionTypes} from "./types";
import * as types from "./types";

const initialState: LoginInfo = {
    id: "",
    isLogin: false,
};

export default function userReducer(
    state = initialState,
    action: UserActionTypes
): LoginInfo {
    switch (action.type) {
        case types.LOGIN:
            return {
                ...state,
                id: action.id,
                isLogin: action.isLogin,
            };
        case types.LOGOUT:
            return {
                ...state,
                id: "",
                isLogin: false
            };
        default:
            return state;
    }
}
