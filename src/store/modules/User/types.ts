export const LOGIN = "user/LOGIN";
export const LOGOUT = "user/LOGOUT";

export type LoginInfo = {
    id: string,
    isLogin?: boolean
}

// 생성자의 반환 타입
export type LoginAction = LoginInfo & {
    type: typeof LOGIN
}

export type LogoutAction = LoginInfo & {
    type: typeof LOGOUT,
}

export type UserActionTypes =
    | LoginAction
    | LogoutAction;