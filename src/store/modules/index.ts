import {combineReducers} from 'redux';
import {LoginInfo, user} from "./User";
import {PostInfo, post} from "./Post";

export type StoreState = {
    user: LoginInfo,
    post: PostInfo
}

export default combineReducers<StoreState>({
    user,
    post
});


// 참고자료 : https://jeonghwan-kim.github.io/dev/2019/07/15/react-redux-ts.html