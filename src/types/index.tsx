export type paramEvent = {
    value: number,
    label: string
}

export type MapItem = {
    idx?: number,
    post_idx?: number,
    title?: string,
    content?: string,
    filePath?: string,
    address?: string, 
    latitude?: number,
    longitude?: number,
    url?: string
}

export type PostItem = {
    idx?: number            // 포스트 id
    category_idx?: number,   // 카테고리 선택 값
    title: string,              // 제목
    url: string,                // url
    urlUpdate?: boolean,        // url 수정여부
    content: string,            // 내용
    star: number,               // 중요도
    lock: number,               // 잠금설정
    expire?: string,             // 마감일
    useExpire?: boolean,
    date?: string,                // 작성일

    mark?: Array<MapItem>         // 마크리스트
}

export type District = {district: string}

// kakao type
export interface Kakao extends Window {
    kakao: any;
}

// declare global {
//     interface Window {
//       kakao: any;
//     }
// }