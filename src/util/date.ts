import {AlarmItem} from "../components/Alarm";

export const yyyymmdd = (date: Date) => {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();
 
    return yyyy + '-' + (mm[1] ? mm : '0'+mm[0]) + '-' + (dd[1] ? dd : '0'+dd[0]);
}

export const yyyymmdd2 = (date: string) => {
    return date.replace(/-/g, '.').slice(0, 10)+'.';
}

export const toMysqlFormat = (date: Date) => {
    // sql에 필요한 Datetime(string) 형태
    return yyyymmdd(date)+' 23:59:59';
}

export const chkExpire = (expire: string) => {
    if(!expire) return false;
    
    let dateDiff = Math.ceil((new Date(expire).getTime()-new Date().getTime())/(1000*3600*24));
    dateDiff = dateDiff - 1;
    if(dateDiff < 0) return true;
    else return false;
}

export const dateSort = (list: Array<AlarmItem>) => {
    return list.sort((a, b)=> a.dday - b.dday)
}