import { store } from '../App';
import * as types from '../store/modules/User/types';
import {AxiosError} from "axios";
import {CLIENT_URL} from "../util/network/config";
import {HOMEPAGE} from "../routes/Links";

export const checkError = (res: AxiosError) => {
    const error = res?.response

    if(error === undefined) return;     // 취소토큰

    switch(error?.status) {
        case 401:
            store.dispatch({type: types.LOGOUT});
            break;
        case 404 :
            alert(res.response?.data.message)
            window.location.href = CLIENT_URL+HOMEPAGE;
            break;
        default:
            alert(error?.data.message);
    }
}