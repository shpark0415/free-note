import axios, {AxiosResponse, AxiosError} from "axios";
import {delType} from "./types";
import {BASEURL} from "./config";

export const del = (params: delType): Promise<AxiosResponse> => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'delete',
            url: BASEURL + params.path,
            data: params.data,
            headers: { Authorization: 'Bearer ' + localStorage.getItem("token") }
        }).then((response: AxiosResponse) => {
            resolve(response);
        }).catch((error: AxiosError) => {
            reject(error);
        })
    });
};
