import axios, {AxiosResponse, AxiosError, CancelTokenSource} from "axios";
import {postType} from "./types";
import {BASEURL} from "../../util/network/config";

export const post = (params: postType): Promise<AxiosResponse> => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: BASEURL + params.path,
            data: params.data,
            headers: { Authorization: 'Bearer ' + localStorage.getItem("token") }
        }).then((response: AxiosResponse) => {
            resolve(response);
        }).catch((error: AxiosError) => {
            reject(error);
        })
    });
};


let cancelToken: CancelTokenSource;
export const postRevocable = async (params: postType): Promise<AxiosResponse> => {
    //Check if there are any previous pending requests
    if (typeof cancelToken != typeof undefined) {
        cancelToken.cancel("Operation canceled due to new request.");
    }

    //Save the cancel token for the current request
    cancelToken = axios.CancelToken.source();

    return await new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: BASEURL + params.path,
            data: params.data,
            headers: { Authorization: 'Bearer ' + localStorage.getItem("token") },
            cancelToken: cancelToken.token  //Pass the cancel token to the current request
        }).then((response: AxiosResponse) => {
            resolve(response);
        }).catch((error: AxiosError) => {
            reject(error);
        })
    });
};
