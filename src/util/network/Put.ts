import axios, {AxiosResponse, AxiosError} from "axios";
import {putType} from "./types";
import {BASEURL} from "../../util/network/config";

export const put = (params: putType): Promise<AxiosResponse> => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'put',
            url: BASEURL + params.path,
            data: params.data,
            headers: { Authorization: 'Bearer ' + localStorage.getItem("token") }
        }).then((response: AxiosResponse) => {
            resolve(response);
        }).catch((error: AxiosError) => {
            reject(error);
        })
    });
};
