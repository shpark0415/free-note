const DEVELOP = 'http://localhost:3005';                // 로컬 개발 서버
const PRODUCTION = 'http://jinmdev.cafe24.com:3005';    // 운영 서버

const CLIENT_DEVELOP = 'http://localhost:3000';   
const CLIENT_PRODUCTION = 'http://jinmdev.cafe24.com:80'

export const BASEURL =
	process.env.REACT_APP_NODE_ENV === 'DEVELOPMENT' ? DEVELOP : PRODUCTION;

export const CLIENT_URL =
	process.env.REACT_APP_NODE_ENV === 'DEVELOPMENT' ? CLIENT_DEVELOP : CLIENT_PRODUCTION;