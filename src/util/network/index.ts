import {get, getRevocable} from './Get';
import {post, postRevocable} from './Post';
import {put} from './Put';
import {del} from './Del';

export {
    post, get, put, del, 
    postRevocable, getRevocable
}
