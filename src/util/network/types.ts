export type postType = {
    path: string,
    data?: object
}

export type getType = {
    path: string,
    data?: object
}

export type putType = {
    path: string,
    data?: object
}

export type delType = {
    path: string,
    data?: object
}